import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './PokemonListDm-styles.js';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<pokemon-list-dm></pokemon-list-dm>
```

##styling-doc

@customElement pokemon-list-dm
*/
export class PokemonListDm extends LitElement {
  static get is() {
    return 'pokemon-list-dm';
  }

  // Declare properties
  static get properties() {
    return {
      url: { type: String, },
      metho: { type: String, },
    };
  }

  // Initialize properties
  

  firstUpdated() {
    this.getData();
  }

  _sendData(data){
    this.dispatchEvent(new CustomEvent('ApiData', {
      detail:{ data }, bubles: true, composed: true
    }));
  }

  getData() {
    fetch(this.url, { method: this.method})
     .then((response) => {
      if(response.ok) return response.json();
      return Promise.reject(response);
    })
    .then((data) => { this._sendData(data); })
    .catch((error) => { console.warn('algo va mal', error); })
    }
  }



  

