import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './PokemonList-styles.js';

import '@bbva-web-components/bbva-button-default/bbva-button-default.js';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<pokemon-list></pokemon-list>
```

##styling-doc

@customElement pokemon-list
*/
export class PokemonList extends LitElement {
  static get is() {
    return 'pokemon-list';
  }

  // Declare properties
  static get properties() {
    return {
      wiki: { type: String, },
      mostrarPokemon: {
        type: String,
        value: 'Mostrar Pokemon',
      },
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.Event = ('ApiData',(e) => {
      this._dataFormat(e.detail.data);
    })
  }

  _dataFormat(data){
    let pokemon = [];

    data["results"].forEach((pokemoon) => {
      pokemon.push({
        abilities:pokemon.abilities,
        name:pokemon.name
      })
    })
    this.wiki = pokemon
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('pokemon-list-shared-styles')
    ];
  }

  // Define a template
  render() {
    return html`
<slot></slot>
<div class="center-horizontal-content">
          <bbva-button-default @click="${this._showPokemon}">
            ${this.t(
              'add-ingredient-button-text',
              this.mostrarPokemon
            )}
          </bbva-button-default>
        </div>
      <get-data url="https://pokeapi.co/api/v2/pokemon/ditto" method="GET"></get-data>

    `;
  }

  
  get dateTemplate(){
    return html`
    ${this.wiki.map(pokemon => html`
    <div class="card">
    <h2>${ pokemon.name }</h2>
    </div>
    `)}
    `;
  }
}
